from setuptools import setup, find_packages

setup(name='sz_xbmc_utils',
      version='0.1',
      description='Utilities for interfacing with a remote XBMC player',
      long_description='',
      classifiers=[
        "Programming Language :: Python",
        ],
      author='',
      author_email='',
      url='',
      keywords='',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=['requests'],
      entry_points='''
      [console_scripts]
      xbmc-videochecker = sz_xbmc_utils.videochecker:main
      ''',
      )

