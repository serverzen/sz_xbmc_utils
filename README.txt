==========
XBMC Utils
==========

The ServerZen XBMC utils are a collection of utilties for working with a local/remote XBMC
server.

Installation
============

Prerequisites
-------------

  * Python 2.7 or higher
  * setuptools

Installation
------------

Recommended install with pip <http://www.pip-installer.org/en/latest/>::

  pip install git+https://bitbucket.org/rockyburt/sz_xbmc_utils.git


Usage
=====

Once installed a new script will be installed, ``xbmc-videochecker``.

Credits
=======

Written and maintained by Rocky Burt <rocky AT serverzen DOT com>


