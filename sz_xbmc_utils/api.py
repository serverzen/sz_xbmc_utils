import json
import requests
import sys


def _pretty(d, indent=0):
    if isinstance(d, dict):
        for key, value in d.iteritems():
            sys.stdout.write(('  ' * indent) + str(key))
            if isinstance(value, dict):
                sys.stdout.write('\n')
                _pretty(value, indent+1)
            elif isinstance(value, (tuple, list)):
                sys.stdout.write('\n')
                _pretty(value, indent+2)
            else:
                if isinstance(value, unicode):
                    v = value.encode('utf-8')
                else:
                    v = str(value)
                sys.stdout.write(': ' + v + '\n')
    elif isinstance(d, (tuple, list)):
        for item in d:
            _pretty(item, indent-1)

class XBMCError(Exception):
    pass

class XBMCCommandError(XBMCError):
    def __init__(self, mname, s):
        self.mname = mname
        self.s = s
        self.message = '(%s) %s' % (self.mname, self.s)

    def __str__(self):
        return self.message

class _XBMC(object):
    def __init__(self, host_and_port):
        self._host_and_port = host_and_port

    @property
    def url(self):
        hp = self._host_and_port
        if ':' not in hp:
            hp += ':8080'
        return 'http://%s/jsonrpc' % hp

    def invoke(self, mname, params=None):
        data = {
            'jsonrpc': '2.0',
            'method': mname,
            'id': 1
            }
        if params:
            data['params'] = params
        r = requests.post(self.url,
                          data=json.dumps(data),
                          headers={'content-type': 'application/json'})
        if 'error' in r.json:
            raise XBMCCommandError(mname, '%i: %s' % (r.json['error']['code'], r.json['error']['message']))

        return r.json['result']

class _Proxy(object):
    def __init__(self, xbmc, name):
        self._xbmc = xbmc
        self._name = name

    def __getattr__(self, s):
        return self.__class__(self._xbmc, self._name + '.' + s)

    def __call__(self, **kwargs):
        return self._xbmc.invoke(self._name, params=kwargs)

class XBMC(object):
    def __init__(self, server):
        self._xbmc = _XBMC(server)

    def __getattr__(self, s):
        return _Proxy(self._xbmc, s)
