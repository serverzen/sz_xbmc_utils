import sys
import os
import textwrap

from . import api
from . import utils


def _print(s=''):
    if isinstance(s, unicode):
        s = s.encode('utf-8')
    sys.stdout.write(str(s))
    sys.stdout.write('\n')
    sys.stdout.flush()

@utils.command(name='tvshows')
def _tvshows(server, basepath):
    '''List movies from filesystem that are not associated with a movie entry within
    xbmc.
    '''

    xbmc = api.XBMC(server)
    _print('Querying XBMC...')
    res = xbmc.VideoLibrary.GetTVShows()['tvshows']
    _print('Retrieving info about %i tv shows...' % len(res))

    tvshows = {}
    for d in res:
        info = xbmc.VideoLibrary.GetTVShowDetails(tvshowid=d['tvshowid'],
                                                 properties=['file'])['tvshowdetails']
        f = info['file']
        if f.endswith('/'):
            f = f[:-1]
        f = os.path.basename(f)
        tvshows[f] = info

    _print('Checking filesystem...')
    for x in os.listdir(basepath):
        if not os.path.isdir(os.path.join(basepath, x)):
            continue

        if x not in tvshows:
            _print('  not registered: ' + x)

    return
    for root, dirs, files in os.walk(basepath):
        for file in files:
            s = file.split('.')
            if s[-1] not in ('avi', 'mp4'):
                continue

            if 'sample' in file.lower():
                continue

            if file.decode('utf-8') in tvshows:
                continue

            _print('  not registered: ' + root[len(basepath)+1:] + '/' + file)


@utils.command(name='movies')
def _movies(server, basepath):
    '''List movies from filesystem that are not associated with a movie entry within
    xbmc.
    '''

    xbmc = api.XBMC(server)
    res = xbmc.VideoLibrary.GetMovies()['movies']
    _print('Retrieving info about %i movies...' % len(res))

    movies = {}
    for d in res:
        info = xbmc.VideoLibrary.GetMovieDetails(movieid=d['movieid'],
                                                 properties=['file'])['moviedetails']
        f = info['file'].split(u'/')[-1]
        movies[f] = info

    _print('Checking filesystem...')
    for root, dirs, files in os.walk(basepath):
        for file in files:
            s = file.split('.')
            if s[-1] not in ('avi', 'mp4'):
                continue

            if 'sample' in file.lower():
                continue

            if file.decode('utf-8') in movies:
                continue

            _print('  not registered: ' + root[len(basepath)+1:] + '/' + file)
    

def main(args=sys.argv[1:]):
    if len(args) == 0:
        _print()
        _print('USAGE: %s <server> <command>' % (os.path.basename(sys.argv[0])))
        _print()
        _print('  commands:')
        wrapper = textwrap.TextWrapper(initial_indent='      ',
                                       subsequent_indent='      ')
        for command in utils.get_commands():
            _print()
            s = '    %s' % command.name
            for arg in command.argspec.args[1:]:
                s += ' <%s>' % arg
            _print(s)
            text = '\n'.join([x.lstrip() for x in command.help.split('\n')])
            _print(wrapper.fill(text))

        _print()

        return

    command = utils.get_command(args[1])
    command.invoke(args[0], *args[2:])

if __name__ == '__main__':
    main()
