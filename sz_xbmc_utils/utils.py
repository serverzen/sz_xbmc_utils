import inspect

class curry:
    def __init__(self, fun, *args, **kwargs):
        self.fun = fun
        self.pending = args[:]
        self.kwargs = kwargs.copy()

    def __call__(self, *args, **kwargs):
        if kwargs and self.kwargs:
            kw = self.kwargs.copy()
            kw.update(kwargs)
        else:
            kw = kwargs or self.kwargs

        return self.fun(*(self.pending + args), **kw)

class CommandInfo(object):
    def __init__(self, callable, name=None):
        self.invoke = callable
        self.argspec = inspect.getargspec(callable)
        self.help = callable.__doc__
        self.name = name or callable.__name__

_commands = {}

def get_command(s):
    return _commands[s]

def get_commands():
    return _commands.values()

def command(*args, **kwargs):
    if len(args) == 1 and callable(args[0]):
        f = args[0]
        f.__command__ = info = CommandInfo(f)
        for k, v in kwargs.items():
            setattr(info, k, v)
        _commands[info.name] = info
        return f
    return curry(command, **kwargs)
